![](https://elioway.gitlab.io/elioflesh/elio-icarus-logo.png)

# icarus ![experimental](/artwork/icon/experimental/favicon.png "experimental")

<https://schema.org> modelled ember app consuming bones and other things the elioWay.

## Prerequisites

You will need the following things properly installed on your computer.

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/) (with npm)
- [Ember CLI](https://ember-cli.com/)

Optionally you can disable the development server and run it against its intended restapi by installing and serving:

- [bones](https://gitlab.com/elioflesh/bones) REST API modeled on <https://schema.org>.

Then disable the production setting for `ENV['ember-cli-mirage']`

## Installation

- `git clone <https://gitlab.com/elioflesh/icarus.git>
- `cd icarus`
- `npm install`

## Running / Development

- `ember serve`
- Visit your app at <http://localhost:4200>.
- Visit your tests at <http://localhost:4200/tests>.

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

- `ember test`
- `ember test --server`

### Linting

- `npm run lint:js`
- `npm run lint:js -- --fix`

### Building

- `ember build` (development)
- `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

Run the bones server:

- [bones](https://gitlab.com/elioflesh/bones) REST API modeled on <https://schema.org>.

## Further Reading / Useful Links

- [ember.js](https://emberjs.com/)
- [ember-cli](https://ember-cli.com/)
- Development Browser Extensions

  - [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  - [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

MIT [Tim Bushell](mailto:tcbushell@gmail.com)

[elioWay](https://gitlab.com/elioway/elio/blob/master/README.md)

![](apple-touch-icon.png)
