import EmberObject from "@ember/object"
import ReslotElementMixin from "icarus/mixins/reslot-element"
import { module, test } from "qunit"

module("Unit | Mixin | reslot-element", function () {
  // Replace this with your real tests.
  test("it works", function (assert) {
    let ReslotElementObject = EmberObject.extend(ReslotElementMixin)
    let subject = ReslotElementObject.create()
    assert.ok(subject)
  })
})
