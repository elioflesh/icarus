import { module, test } from "qunit"
import { setupTest } from "ember-qunit"

module("Unit | Adapter | thing", function (hooks) {
  setupTest(hooks)

  // Keep this test that always passes.
  test("it exists", function (assert) {
    let adapter = this.owner.lookup("adapter:thing")
    assert.ok(adapter)
  })

  test("it is brilliant", function (assert) {
    let adapter = this.owner.lookup("adapter:thing")
    assert.deepEqual(adapter.namespace, "engage")
    assert.deepEqual(adapter.host, "http://localhost:3030")
  })
})
