import { module, test } from "qunit"
import { setupTest } from "ember-qunit"

module("Unit | Model | thing", function (hooks) {
  setupTest(hooks)

  // Replace this with your real tests.
  test("it exists", function (assert) {
    let store = this.owner.lookup("service:store")
    let model = store.createRecord("thing", {
      name: "I am Name",
      disambiguatingDescription: "I am not",
    })
    assert.ok(model, "model not null")
    assert.equal(model.name, "I am Name", "name")
    assert.equal(
      model.disambiguatingDescription,
      "I am not",
      "disambiguatingDescription"
    )
  })
})
