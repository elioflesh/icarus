import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { click, render } from "@ember/test-helpers"
import hbs from "htmlbars-inline-precompile"

var SHORT_NAME = "test short name"

module("Integration | Component | button-with-confirmation", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    let testHook = function () {}
    this.set("testHook", testHook)
    this.set("testShortName", SHORT_NAME)
    // this.set('thing', 1);

    // Template block usage:
    await render(hbs`
        {{#button-with-confirmation shortName=testShortName confirmHook=testHook thing=thing }}
          test block text
        {{/button-with-confirmation}}
      `)

    // Block text hidden by default
    assert.equal(
      this.element.firstElementChild.tagName,
      "SPAN",
      "parented by span"
    )
    assert.equal(this.element.textContent.trim(), SHORT_NAME)
    assert.equal(
      this.element.querySelector("button").textContent.trim(),
      SHORT_NAME,
      "starts showing shortName"
    )
    assert.equal(
      this.element.querySelectorAll("button").length,
      1,
      "starts showing 1 button"
    )
  })

  test("clicking the confirm toggle button", async function (assert) {
    let testHook = function () {}
    this.set("testHook", testHook)
    this.set("testShortName", SHORT_NAME)
    this.set("thing", 1)

    await render(hbs`
        {{#button-with-confirmation shortName=testShortName confirmHook=testHook thing=thing }}
          test block text
        {{/button-with-confirmation}}
      `)

    await click("button")

    assert.equal(
      this.element.textContent.replace(/[^a-zA-Z ]/g, "").trim(),
      "Cancel            test block text OK"
    )
    assert.equal(
      this.element.querySelectorAll("button").length,
      2,
      "toggles, showing 2 buttons"
    )
    assert.equal(
      this.element.querySelector("button").textContent.trim(),
      "Cancel",
      'toggles, showing "Cancel"'
    )

    await click("button")

    assert.equal(
      this.element.querySelectorAll("button").length,
      1,
      "toggles, showing 2 buttons"
    )
    assert.equal(
      this.element.querySelector("button").textContent.trim(),
      SHORT_NAME,
      "toggles, showing shortName"
    )

    await click("button")

    assert.equal(
      this.element.querySelectorAll("button").length,
      2,
      "toggles, showing 1 button"
    )
    assert.equal(
      this.element.querySelector("button").textContent.trim(),
      "Cancel",
      'toggles, showing "Cancel"'
    )
  })

  test("callback", async function (assert) {
    assert.expect(2)

    let testHook = function () {
      return new Promise(function (resolve) {
        assert.step("Hook is called!")
        resolve()
      })
    }
    this.set("testHook", testHook)
    this.set("thing", 1)

    await render(hbs`
          {{#button-with-confirmation confirmHook=testHook thing=thing }}
          {{/button-with-confirmation}}
        `)
    // click the first button
    await click("button")

    // click the second button
    await click(this.element.querySelectorAll("button")[1])

    assert.verifySteps(["Hook is called!"])
  })
})
