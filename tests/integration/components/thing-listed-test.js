import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { click, render } from "@ember/test-helpers"
import hbs from "htmlbars-inline-precompile"

module("Integration | Component | thing-listed", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    await render(hbs`{{thing-listed}}`)

    assert.equal(
      this.element.firstElementChild.tagName,
      "LI",
      "the correct parent element"
    )
    assert.equal(
      this.element.textContent.replace(/[^a-zA-Z ]/g, "").trim(),
      "EditDel"
    )
  })

  test("data rendered", async function (assert) {
    var oneOf = {
      id: "abc123",
      name: "abc",
    }
    this.set("thing", oneOf)

    await render(hbs`
        {{#thing-listed thing=thing}}{{/thing-listed}}
      `)

    assert.equal(
      this.element.textContent.replace(/[^a-zA-Z ]/g, "").trim(),
      "abcEditDel",
      "correct data"
    )
  })

  test("callback confirmHook registered", async function (assert) {
    assert.expect(2)

    var oneOf = {
      id: "abc123",
      name: "abc",
    }
    let testHook = function () {
      return new Promise(function (resolve) {
        assert.step("Hook is called!")
        resolve()
      })
    }
    this.set("thing", oneOf)
    this.set("testHook", testHook)

    await render(hbs`
        {{#thing-listed thing=thing confirmHook=testHook }}
        {{/thing-listed}}
      `)

    await click("button")
    await click(this.element.querySelectorAll("button")[1])
    assert.verifySteps(["Hook is called!"])
  })

  test("callback confirmHook called with parameters", async function (assert) {
    assert.expect(1)

    var oneOf = {
      id: "abc123",
      name: "abc",
    }
    let testHook = function (actual) {
      return new Promise(function (resolve) {
        assert.equal(actual.id, "abc123")
        resolve(actual)
      })
    }
    this.set("thing", oneOf)
    this.set("testHook", testHook)

    await render(hbs`
        {{#thing-listed thing=thing confirmHook=testHook }}
        {{/thing-listed}}
      `)

    await click("button")
    await click(this.element.querySelectorAll("button")[1])
  })
})
