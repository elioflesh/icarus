import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import hbs from "htmlbars-inline-precompile"

module("Integration | Component | things-list", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    await render(hbs`{{things-list}}`)

    assert.equal(
      this.element.firstElementChild.tagName,
      "UL",
      "the correct parent element"
    )
    assert.equal(this.element.textContent.trim(), "")
  })

  test("data rendered", async function (assert) {
    var listOf = [
      {
        id: "abc123",
        name: "abc",
      },
      {
        id: "def456",
        name: "def",
      },
      {
        id: "ghi789",
        name: "ghi",
      },
    ]
    this.set("list", listOf)

    await render(hbs`
      {{#things-list model=list}}
      {{/things-list}}
    `)

    assert.equal(
      this.element.querySelectorAll("li").length,
      3,
      "the correct number"
    )
    assert.equal(
      this.element
        .querySelector("li")
        .textContent.replace(/[^a-zA-Z ]/g, "")
        .trim(),
      "abcEditDel",
      "the first one with correct data"
    )
  })
})
