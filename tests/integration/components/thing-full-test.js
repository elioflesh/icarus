import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import hbs from "htmlbars-inline-precompile"

module("Integration | Component | thing-full", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    await render(hbs`{{thing-full}}`)

    assert.equal(
      this.element.firstElementChild.tagName,
      "DL",
      "the correct parent element"
    )
    assert.equal(
      this.element.querySelectorAll("dd").length,
      2,
      "with all fields"
    )
  })

  test("data rendered", async function (assert) {
    var oneOf = {
      id: "abc123",
      name: "abc",
      disambiguatingDescription: "The 123 of abc",
    }
    this.set("thing", oneOf)

    await render(hbs`
          {{#thing-full thing=thing}}{{/thing-full}}
        `)

    assert.equal(
      this.element.querySelectorAll("dd").length,
      2,
      "with all fields "
    )
    assert.equal(
      this.element.querySelector("dt").textContent.trim(),
      "abc",
      "the first dt with name data"
    )
    assert.equal(
      this.element.querySelectorAll("dd")[0].textContent.trim(),
      "The 123 of abc",
      "the first field with disambiguatingDescription data"
    )
    assert.equal(
      this.element.querySelectorAll("dd")[1].textContent.trim(),
      "abc123",
      "the second field with id data"
    )
  })
})
