import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { click, fillIn, render } from "@ember/test-helpers"
import hbs from "htmlbars-inline-precompile"

module("Integration | Component | thing-form", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    let testHook = function () {}
    this.set("testHook", testHook)

    await render(hbs`
          {{#thing-form submitHook=testHook }}
          {{/thing-form}}
          `)

    assert.equal(
      this.element.firstElementChild.tagName,
      "FORM",
      "the correct parent element"
    )
    assert.equal(
      this.element.querySelectorAll("input").length,
      2,
      "with all fields"
    )
  })

  test("it renders brilliantly", async function (assert) {
    let testHook = function () {}
    this.set("testHook", testHook)

    await render(hbs`
          {{#thing-form formLabel='Add' submitHook=testHook }}
          {{/thing-form}}
        `)

    assert.equal(
      this.element.querySelector("button").textContent.trim(),
      "Add",
      "with the form button is labelled"
    )
  })

  test("data rendered", async function (assert) {
    var oneOf = {
      id: "abc123",
      name: "abc",
      disambiguatingDescription: "The 123 of abc",
    }
    let testHook = function () {}
    this.set("model", oneOf)
    this.set("testHook", testHook)

    await render(hbs`
          {{#thing-form model=model submitHook=testHook }}
          {{/thing-form}}
        `)

    assert.equal(
      this.element.querySelectorAll("input")[0].value,
      "abc",
      "the first field with name data"
    )
    assert.equal(
      this.element.querySelectorAll("input")[1].value,
      "The 123 of abc",
      "the second field with disambiguatingDescription data"
    )
  })

  test("form submits", async function (assert) {
    assert.expect(2)

    var oneOf = {
      name: "",
      disambiguatingDescription: "I'm the wizard!",
    }
    let testHook = function () {
      assert.deepEqual(
        oneOf.name,
        "You are not a wizard1!",
        "submitted value is passed to external action"
      )
      assert.deepEqual(
        oneOf.disambiguatingDescription,
        "You are not a wizard2!",
        "disambiguatingDescription value stuck at original"
      )
    }
    this.set("model", oneOf)
    this.set("testHook", testHook)

    await render(hbs`
          {{#thing-form model=model submitHook=testHook }}
          {{/thing-form}}
        `)

    await fillIn(
      this.element.querySelectorAll("input")[0],
      "You are not a wizard1!"
    )
    await fillIn(
      this.element.querySelectorAll("input")[1],
      "You are not a wizard2!"
    )

    await click("button")
  })
})
