import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { module, test } from "qunit"
import { click, currentURL, fillIn, visit } from "@ember/test-helpers" //
import { setupApplicationTest } from "ember-qunit"

module("Acceptance | crud", function (hooks) {
  setupApplicationTest(hooks)
  setupMirage(hooks)

  test("should index existing things", async function (assert) {
    let things = server.createList("thing", 3)

    await visit("/")

    assert.equal(currentURL(), "/")

    assert.equal(
      this.element.querySelectorAll("ul li").length,
      3,
      "and list 3 items"
    )

    for (let i = 0; i < 3; i++) {
      assert.equal(
        this.element
          .querySelectorAll("ul li")
          [i].textContent.replace(/[^a-zA-Z ]/g, ""),
        things[i].name + "EditDel",
        "and show the item names"
      )
    }
  })

  test("should view a thing", async function (assert) {
    let things = server.createList("thing", 3)
    let thing = things[0]

    await visit("/thing/" + thing.id + "/view")

    assert.equal(currentURL(), "/thing/" + thing.id + "/view")

    assert.equal(
      this.element.querySelectorAll("dd").length,
      2,
      "with all fields"
    )
    assert.equal(
      this.element.querySelector("dt").textContent.trim(),
      thing.name,
      "with the first dt with name data"
    )
    assert.equal(
      this.element.querySelectorAll("dd")[0].textContent.trim(),
      thing.disambiguatingDescription,
      "the first field with disambiguatingDescription data"
    )
    assert.equal(
      this.element.querySelectorAll("dd")[1].textContent.trim(),
      thing.id,
      "the second field with id data"
    )
  })

  test("should CReate new thing", async function (assert) {
    await visit("/new")

    assert.equal(currentURL(), "/new")

    await fillIn("input", "Zinc Thing")
    await click("form button")

    assert.equal(
      this.element.querySelectorAll("dd").length,
      2,
      "ends up in full view"
    )
    assert.equal(
      this.element.querySelector("dt").textContent.trim(),
      "Zinc Thing",
      "with the first dt with name data"
    )
    assert.equal(
      this.element.querySelectorAll("dd")[0].textContent.trim(),
      "",
      "the first field with empty disambiguatingDescription data"
    )
    assert.ok(
      this.element.querySelectorAll("dd")[1].textContent.trim(),
      "the second field with id data"
    )
  })

  test("can abandon CReate new thing", async function (assert) {
    await visit("/new")

    assert.equal(currentURL(), "/new")

    await fillIn("input", "Zinc Thing")

    assert.equal(
      this.element.querySelectorAll("ul li").length,
      0,
      "and the total number in the list will not change"
    )

    await click("form button")

    assert.equal(
      this.element.querySelectorAll("ul li").length,
      1,
      "and the total number in the list will change once submitted"
    )
  })

  test("should Update a thing", async function (assert) {
    let things = server.createList("thing", 3)
    let thing = things[0]

    await visit("/thing/" + thing.id + "/edit")

    assert.equal(currentURL(), "/thing/" + thing.id + "/edit")

    await fillIn("input", "Zinc Thing")
    await click("form button")

    assert.equal(currentURL(), "/thing/" + thing.id + "/view")

    assert.equal(
      this.element.querySelectorAll("ul li").length,
      3,
      "and the total number in the list will not change"
    )

    assert.equal(
      this.element
        .querySelectorAll("ul li")[0]
        .textContent.replace(/[^a-zA-Z ]/g, ""),
      "Zinc ThingEditDel",
      "and the Updated thing will have the correct details"
    )
  })

  test("should Delete a thing", async function (assert) {
    let things = server.createList("thing", 3)

    await visit("/")

    assert.equal(currentURL(), "/")

    await click(this.element.querySelectorAll("button")[0])
    await click(this.element.querySelectorAll("button")[1])

    assert.equal(currentURL(), "/", "not be redirected")

    assert.equal(
      this.element.querySelectorAll("ul li").length,
      things.length - 1,
      "but remove one of the items from the list"
    )
  })
})
