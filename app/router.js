import EmberRouter from "@ember/routing/router"
import config from "./config/environment"

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL,
})

Router.map(function () {
  this.route(
    "things",
    {
      path: "/",
    },
    function () {
      this.route("new")
      this.route(
        "thing",
        {
          path: "thing/:thing_id",
        },
        function () {
          this.route("edit")
          this.route("view")
        }
      )
    }
  )
})

export default Router
