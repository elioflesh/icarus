import Mixin from "@ember/object/mixin"

export default Mixin.create({
  didRender() {
    this._super(...arguments)
    // can't do this - so this mixin is pretty useless.
    // but it's how you do mixins!
    // this.$().insertBefore("h1");
  },
})
