import Route from "@ember/routing/route"

export default Route.extend({
  saveThing: function () {
    var self = this
    let thing = this.modelFor(this.routeName)
    thing
      .save()
      .then(function () {
        self.transitionTo("things.thing.view", thing)
      })
      .catch(function (reason) {
        return reason
      })
  },
  delThing: function () {
    var self = this
    let thing = this.modelFor(this.routeName)
    thing
      .destroyRecord()
      .then(function () {
        self.transitionTo("things")
      })
      .catch(function (reason) {
        return reason
      })
  },
  actions: {
    saveThingAction() {
      this.saveThing()
    },
    delThingAction() {
      this.delThing()
    },
  },
})
