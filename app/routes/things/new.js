import Route from "@ember/routing/route"

export default Route.extend({
  model() {
    let imathing = this.store.createRecord("thing")
    return imathing
  },
  createThing: function () {
    var self = this
    let thing = this.modelFor(this.routeName) // or in a controller:: `this.model`
    thing
      .save()
      .then(function () {
        self.transitionTo("things.thing.view", thing)
      })
      .catch(function (reason) {
        return reason
      })
  },
  actions: {
    createThingAction() {
      this.createThing()
    },
  },
})
