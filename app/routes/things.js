import Route from "@ember/routing/route"

export default Route.extend({
  model() {
    return this.store.findAll("thing")
  },
  destroyThing: function (thing) {
    var self = this
    return thing
      .destroyRecord()
      .then(function () {
        self.transitionTo("things")
      })
      .catch(function (reason) {
        return reason
      })
  },
  actions: {
    destroyThingAction(thing) {
      this.destroyThing(thing)
    },
  },
})
