import ReslotElementMixin from "../mixins/reslot-element"
import Component from "@ember/component"

export default Component.extend(ReslotElementMixin, {
  tagName: "form",
  formLabel: "Update",
  actions: {
    submitHook() {
      this.submitHook({ comment: this.name })
    },
  },
})
// {{action "submitHook" on="submit"}}
