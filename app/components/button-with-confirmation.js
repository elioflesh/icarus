import Component from "@ember/component"

export default Component.extend({
  tagName: "span",
  confirmShown: false,
  shortName: 'required: shortName="xxx"',
  _toggleText: "<shortName required>",
  actions: {
    toggleConfirmDialog() {
      this.toggleProperty("confirmShown")
      this.set(
        "_toggleText",
        this.get("confirmShown") ? "Cancel" : this.get("shortName")
      )
    },
  },
  didRender() {
    this.set(
      "_toggleText",
      this.get("confirmShown") ? "Cancel" : this.get("shortName")
    )
  },
})
