import ReslotElementMixin from "../mixins/reslot-element"
import Component from "@ember/component"

export default Component.extend(ReslotElementMixin, { tagName: "dl" })
