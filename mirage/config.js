export default function () {
  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  this.urlPrefix = "http://localhost:3030" // make this `http://localhost:8080`, for example, if your API is on a different server
  this.namespace = "/engage" // make this `/api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  // this.get('/things', (schema, request) => {
  //   return schema.things.all();
  // });
  this.get("/things")
  this.post("/things")
  this.get("/things/:id")
  this.patch("/things/:id") // or this.patch
  this.del("/things/:id")
}
