# Nest the routes vs not nested routes:

```
// router.js
// Flattened:
Router.map(function() {
  this.route("things");
  this.route("new");
  this.route("edit", { path: "edit/:thing_id" });
  this.route("view", { path: "view/:thing_id" });
  );
});

// router.js
// Not nested:
Router.map(function() {
  this.route("things");
  this.route("new");
  this.route("thing", { path: "thing/:thing_id" }, function() {
        this.route("edit");
        this.route("view");
      }
  );
});

// Nested:
Router.map(function() {
  this.route("things", function() {
    this.route("new");
    this.route("thing", { path: "thing/:thing_id" }, function() {
        this.route("edit");
        this.route("view");
      }
    );
  });
});
```

## All templates

```
// Flattened:
{{#link-to 'edit'
// Not nested:
{{#link-to 'thing.edit'
// Nested:
{{#link-to 'things.thing.edit'
```

## All tests which call those routes, including route, controller and acceptance tests:

```
// Flattened:
lookup('route:edit....')
// Not nested:
lookup('route:thing/edit....')
// Nested:
lookup('route:things/thing/edit....')
```

## Folders:

Flattened:

- `new.js` + `thing.js` + `edit.js` + `view.js` files alongside `things.js`
- no `things` or `thing` folder in [test/]app/routes or [test/]app/controllers.

Not nested in app and tests:

- Reverse settings in "Nested in app and tests".

Nested in app and tests:

- [test/]app/routes and also [test/]app/controllers

  - insert a "things" folder and move the following files into it:

    - `new.js` + `thing.js` files
    - `thing` folder with `edit.js` + `view.js`
