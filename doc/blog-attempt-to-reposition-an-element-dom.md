# Reslot: Obeying elioSin's 6th commandment.

See the reslot mixin and its implementation.

We are doing this to implement the sin framework, whose 6th Commandment is: A "**sinner** cannot rise above the first **pillar** tag _below_ it."

Sin floats things to the left and right. When a form shows I want it to float to the highest position in heaven it can, not necessarily alongside the "first **pillar** tag _below_ it."

But I wrote the rules, so the solution is to use the rules and reposition the form element - aka "component" in ember - above the h1 tag - which is sure to be high up.

This mixin achieves that - but I don't think ember likes it.
