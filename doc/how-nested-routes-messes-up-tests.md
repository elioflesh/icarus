# Nested route issues

```
ember test --filter="Acceptance | crud"
```

2 fails:

```
actual: >
    /things/new
expected: >
    /things
stack: >
        at Object.<anonymous> (http://localhost:7357/assets/tests.js:46:14)
message: >
     then redirect to the list
negative: >
    false
Log: |


not ok 4 Chrome 68.0 - [191 ms] - Acceptance | crud: should Update a thing


actual: >
    /things/thing/abcDEF1910728687/edit
expected: >
    /things
stack: >
        at Object.<anonymous> (http://localhost:7357/assets/tests.js:64:14)
message: >
    then redirect to the list
negative: >
    false
Log: |

ok 5 Chrome 68.0 - [207 ms] - Acceptance | crud: should Delete a thing

1..5
# tests 5
# pass  3
# skip  0
# fail  2
```

The reason these tests fail is that, within the context of the whole app, two of our tests simulate a button click. The ember helper `click` is what I call lazy and `click('button')` will click the first 'button' in the DOM. The tests at component level work fine because - within the context of the test - the first button happens to be the _only_ button.

The solution is to firm up the way we select the button in our Acceptance test.

Edit: elioThing/icarus/tests/acceptance/crud-test.js

- Change in "Acceptance | crud: should Delete a thing":

  - `await click("button")` to `await click("form button")`;

- Change in "Acceptance | crud: should Update a thing":

  - `await click("button")` to `await click("form button")`;

::

ember test --filter="Acceptance | crud"

Tah dah! No errors. Changes to these tests will also survive changing the nesting of routes.

# Alternative solution useful to know.

Another way would be to click the result of a call to the `querySelectorAll` method on the element in context. The `querySelector` method is lazy just like `click`.

In fact `click('button')` is like saying

```
click(
    this.element.querySelector('button')
    )
```

`querySelectorAll` is not lazy, but neither can you be because you have to choose the element you want to click from the selector list - you cannot lazily allow `querySelector` to select the first.

```
click(
    // the last button
    this.element.querySelectorAll('form button')[0]
)
```

# Alternative view. This is not a problem with our tests

Future proofing your Acceptance tests against rigorous changes like nesting the apps routes differently, guides you to more modular components. At the same time some changes can't be protected against, and it's your test's responsibility to highlight potential issues.

Whether you are using `querySelectorAll` or `querySelector` to click a button in an acceptance test this is making an assumptions about the DOM state on that route.

By making the change we did we're telling our tests that we don't care if the other "things" are listed on the page or not (they also have buttons which could be selected to click).

`click('form button')` assumes that the first button inside a form in the active DOM is the action button we want our user to click. It doesn't matter how many other buttons there are on the page. So if it doesn't matter to you how many other buttons there are on the page, then this selector is fine. It only matters to you how many _forms_ there are.

`click('button')` may be failing because the nested route you have created means a confusing number of buttons are on display to the user (just as a confusing number of buttons are available to the lazy `click` function). Sometimes lazy is like Occam's Razor.

Perhaps we should fix this test by changing the list view so that it doesn't show a del button, but something less buttony and confusing, when the form is showing. Test can lead to better solutions.
